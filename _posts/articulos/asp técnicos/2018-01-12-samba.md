---
date: 2017-11-17
type: Document
title:  "Samba"
description: "Sistema para compartir contenido con Windows"
tags:
    - deepin-aspectos técnicos
    - windows
    - samba
---

El Dynamic Host Configuration Protocol es un sistema de gestión de dirección dinámicas.

## Lectura adicional
* [Wikipedia](https://es.wikipedia.org/wiki/Samba_(software))
* [Guía de aplicaciones]({{ site.url }}{{ site.baseurl }}/deepin/guiapps/)
* [Página web oficial](https://www.samba.org/)
* [Debian](https://packages.debian.org/samba)
