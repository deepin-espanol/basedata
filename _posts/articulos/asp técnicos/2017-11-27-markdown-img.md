---
type: Document
date: 2017-10-02
title: "Imágenes y Markdown"
description: "Dando forma al artículo con imágenes"
set: tutopost
set_order: 5  
tags:
    - deepin-aspectos técnicos
    - markdown
    - post
    - jekyll
    - tutopost
---

Ayuda de Deepin usa un formato de posts basada en Markdown. Aquí explicaremos a incrustar una imágen.

## Pasos
1. Si subes por primera vez, hazo en la carpeta `images` del [repositorio]({{site.data.externalurl.reposource}}).
2. Dirígete al post que está creando o editando.
3. Añade este código `{ % include imagen source="foto.jpg" alt="Descripción de imagen" %}`
4. Establece la ruta de la foto y la descripción, los detalles abajo.

## Explicación
~~~
Base: { % include imagen source="foto.jpg" alt="Descripción de imagen" %}
Ejemplo: {% include imagen source="screenshots/kde-connect.png" alt="El archivo config." %}
~~~

## Explicación técnica
### Por qué no el código Markdown
En general, las imágenes usan el mismo formato de los enlaces, solo que añadimos un signo de interrogación:

~~~
![Descripción de imagen]({{site.url}}/images/foto.jpg)
~~~

El problema es que la imagen se encajará con la página. Nuestro consejo es usar este código anterior en su lugar.

### Por qué no "img"

Usamos la etiqueta `img` para añadir la etiqueta `class` (que arregla la ubicación de la imagen) y `data-src` para mostrar al finalizar la carga.

~~~
<img class="screenshot" data-src="{{site.url}}/images/foto.jpg" alt=Descripción de imagen">
~~~

## Lectura adicional

* [Sitio web de Markdown en español](https://markdown.es/)
