---
date: 2017-11-17
type: Document
title:  "Sistema de ventanas X"
description: "La proyección de la pantalla"
tags:
    - deepin-aspectos técnicos
    - windows
    - samba
---

El Sistema de ventanas X es el nombre del software que da vida al entorno gráfico de Linux. Fue desarrollado en la década 1980 y es actualmente desarrollada por la X.Org Foundation. Es la base del entorno gráfico, véase DDE.

{% include imagen source="graficos/Esquema_de_las_capas_de_la_interfaz_gráfica_de_usuario_por_Otto_Traian" alt="Maratón Linuxero" %}

Crédito: [Otto Traian](https://es.wikipedia.org/wiki/Archivo:Esquema_de_las_capas_de_la_interfaz_gr%C3%A1fica_de_usuario.svg) de Wikimedia Commons, licenciado bajo Creative Commons.

## Lectura adicional
* [Wikipedia](https://es.wikipedia.org/wiki/Servidor_X)
* [Página web oficial](http://www.x.org/)
* [Controladores gráficos]({{ site.url }}{{ site.baseurl }}/deepin/drivers/)
* [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Xorg-Server-169-1610-Displays)
