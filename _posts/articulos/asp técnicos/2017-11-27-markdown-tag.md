---
type: Document
date: 2017-10-02
title: "Etiquetas y Markdown"
description: "Dando forma al artículo con etiquetas"
set: tutopost
set_order: 6   
tags:
    - deepin-aspectos técnicos
    - markdown
    - post
    - jekyll
    - tutopost
---

Ayuda de Deepin usa un formato de posts basada en Markdown. Aquí explicaremos a usar correctamente las etiquetas una imágen.

## Encabezado
Debes localizar la etiqueta tags al crear o editar post:

~~~
---
date:
title:
tags: [aquí]
description:
type: Document
---
~~~

## Uso de etiquetas
1. Añade una etiqueta con el prefijo `deepin-`
  - Es la parte principal, y lo tratamos como categoría.
2. Añade otras etiquetas
  - Cada etiqueta lleva un guíon medio al comenzar
3. Si es un vídeo complementa con `videogalería`

### Categorías permitidas por tipo
* Categorías indispensables como información del sistema, tutoriales y FAQ:
	- `deepin-sistema operativo` (ubicado en la carpeta `SO`)
	- `deepin-tutoriales` (pasos básicos)
  - `deepin-faq` (cada artículo forma parte de "preguntas y respuestas")
* Categorías adicionales, tips, información avanzada y miscelánea:
	- `deepin-tips y trucos` (ubicado en la carpeta `tips y trucos`)
	- `deepin-aspectos técnicos` (con tilde y considerando espacio)
  - `deepin-miscelánea` (con tilde)
* Categorías de aplicaciones, dividio en general, juegos y utilidades para avanzados:
	- `apps-general` (ubicado en la carpeta `apps`)
	- `apps-juegos` (carpeta separada con `juegos`)
	- `apps-utilidades` (carpeta separada con `utilidades`)

## Lectura adicional

* [Sitio web de Markdown en español](https://markdown.es/)
