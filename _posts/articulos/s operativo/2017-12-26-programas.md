---
date: 2017-11-17
type: Document
title:  "Programas en Deepin"
description: "Introducción a los programas y cómo usarlos"
tags:
    - deepin-sistema operativo
    - sofware
    - instalacion
    - primeros pasos
    - aplicaciones
set: importante
set_order: 7
icon: whatshot
---

Para complementar los primeros pasos en usar Deepin, te ofrecemos anexos sobre los programas (o aplicaciones) compatibles con Deepin (y otras distribuciones GNU/Linux).

## Antes de comenzar
Te animamos a conocer estos artículos para conocer mejor el uso de las aplicaciones
* [Tienda de aplicaciones]({{ site.url }}{{ site.baseurl }}/deepin/store/): Descarga las aplicaciones que quieras
* [Espejos]({{ site.url }}{{ site.baseurl }}/deepin/espejos/): Cambiando a un repositorio veloz para conseguir lo último en actualizaciones
* [Instalar aplicaciones]({{ site.url }}{{ site.baseurl }}/deepin/instalar-apps/): Instalación de aplicaciones, formas y recomendaciones

## Lista de programas GNU/Linux
* [Aplicaciones preinstaladas]({{ site.url }}{{ site.baseurl }}/deepin/appspreinstaladas/)
* Guías
  - [Aplicaciones en general]({{ site.url }}{{ site.baseurl }}/deepin/guiapps/)
  - [Aplicaciones por profesión]({{ site.url }}{{ site.baseurl }}/deepin/appsprofesion/)
  - [Aplicaciones para desarrolladores]({{ site.url }}{{ site.baseurl }}/deepin/appsdesarrollo/)
  - [Juegos en Deepin]({{ site.url }}{{ site.baseurl }}/deepin/juegos/)

## Programas no nativos
### Microsoft Windows
* [Instalar aplicaciones de Windows]({{ site.url }}{{ site.baseurl }}/deepin/exeapps/)
* Crossover
  - [Información]({{ site.url }}{{ site.baseurl }}/deepin/crossover/)
  - [Trucos Crossover]({{ site.url }}{{ site.baseurl }}/deepin/tips-crossover/)
* PlayonLinux
  - [Información]({{ site.url }}{{ site.baseurl }}/deepin/playonlinux/)
  - [Trucos PlayonLinux]({{ site.url }}{{ site.baseurl }}/deepin/playonlinuxapps/)

## Lectura adicional
* [Página dedicada a la instalación]({{ site.url }}{{ site.baseurl }}/deepin/instalacion/)
* [Página dedicada a las actualizaciones]({{ site.url }}{{ site.baseurl }}/deepin/update/)
