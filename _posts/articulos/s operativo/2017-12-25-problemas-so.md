---
date: 2017-11-17
type: Document
title:  "Problemas en Deepin"
description: "Páginas de ayuda para situaciones críticas"
tags:
    - deepin-sistema operativo
    - bug
    - dde
    - problemas
icon: control
set: importante
set_order: 8
---
¿Tienes un problema en Deepin? ¿Quieres saber por qué ocurre? Esta lista contiene información para solucionar problemas relacionados al sistema operativo.

Nota: Al menos que tengas un convenio de soporte con Deepin Wuhan Tecnology, **los aportes mostrados a continuación no tienen garantía**. Eres responsable en reparar este sistema operativo.

Nota 2: Si no puedes iniciar Deepin, revisa **[la guía para solucionar los problemas frecuentes]({{ site.url }}{{ site.baseurl }}/deepin/badload/)**.

## Lista de problemas
### Arranque
* [Problemas de arranque]({{ site.url }}{{ site.baseurl }}/deepin/badload/)
* [BIOS]({{ site.url }}{{ site.baseurl }}/deepin/bios/)

### Paquetes
* [Problemas al configurar paquetes]({{ site.url }}{{ site.baseurl }}/deepin/problemas-conf-paquetes/)
* [Problemas con la configuración al actualizar paquetes]({{ site.url }}{{ site.baseurl }}/deepin/actualizar-conf-paquetes/)
* [Llaves GPG]({{ site.url }}{{ site.baseurl }}/deepin/llaves-gpg/)

### Audio
* [Problema con el controlador de audio]({{ site.url }}{{ site.baseurl }}/deepin/fix-audio/)

### Gráficos
* [Instalar controlador Bumblebee]({{ site.url }}{{ site.baseurl }}/deepin/install-bumblebee-nvidia/)

### Conexión
* [Arreglos Wifi]({{ site.url }}{{ site.baseurl }}/deepin/wififix/)
* [Arreglos Bluethoot]({{ site.url }}{{ site.baseurl }}/deepin/bluethootfix/)

### Consumo de energía
* [Herramientas para ahorro de energía]({{ site.url }}{{ site.baseurl }}/deepin/ahorro-energia/)

## Lista de problema con aplicaciones
* [Virtualbox]({{ site.url }}{{ site.baseurl }}/deepin/fix-kernel-virtualbox/)

## Lectura adicional
* [Deepin Recovery]({{ site.url }}{{ site.baseurl }}/deepin/deepin-recovery/)
* [Hacer el disco de arranque]({{ site.url }}/deepin/discoarranque/)
