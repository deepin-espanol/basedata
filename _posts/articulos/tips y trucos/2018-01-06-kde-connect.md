---
date: 2017-11-17
type: Document
title:  "Instalar KDE Connect"
description: "Cómo instalar y usar esa característica"
comments: yes
tags:
    - deepin-tips y trucos
    - fix
    - dialogo
    - apps
---

Este apunte explica como usar KDE Connect en Deepin. Si bien es dependiente de plasma, este servicio usa pocas librerías y no genera conflicto.

## Pasos
1. Instalar el repositorio oficial (desde [Google+](https://plus.google.com/106731706454776586722/posts/hxrdpk2EHJG))
2. Indicator KDE Connect con `sudo install indicator-kdeconnect`
3. Ejecutar "KDE Connect Indicator"
  - Si tienes problemas con la ejución, verifica si hay un conflicto entre dependencias con `sudo apt install -f`
4. Emparejar el dispositivo con la aplicación compatible

{% include imagen source="screenshots/kde-connect.png" alt="El archivo config." %}

## Aplicación para móvil
Solo para Android: [Instalar desde Play Store](https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp)

## Fuentes
* [Google+](https://plus.google.com/106731706454776586722/posts/hxrdpk2EHJG)
* [Blog](http://www.caroblog.it/2017/11/02/install-kde-connect-debian-deepin-linux/)
