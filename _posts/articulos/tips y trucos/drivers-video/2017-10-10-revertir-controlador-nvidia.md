---
date: 2017-11-17
type: Document
title:  "Revertir a controlador Nouveau (Nvidia)"
description: "Cómo arreglar la tarjeta Nvidia mal instalada"
tags:
    - deepin-tips y trucos
    - driver
    - nvidia
    - boot
---
Esta página explica como revertir el controlador de Nvidia a Noveau [en modo TTY]({{ site.url }}{{ site.baseurl }}deepin/tty/). Es un complemento a la página de [problemas sobre Nvidia]({{ site.url }}{{ site.baseurl }}deepin/badload-nvidia/).

## Pasos para retornar al driver libre Nouveau
Primero. Hacemos una copia de seguridad con [cd]({{ site.url }}{{ site.baseurl }}deepin/archivos-terminal/). `sudo mv /etc/X11/xorg.conf /etc/X11/xorg.conf.BACKUP`.:

Segundo. Tenemos dos posibilidades para retomar el driver Nouveau:
* `sudo apt install nouveau-firmware && sudo dpkg-reconfigure xserver-xorg`
* `sudo apt install xserver-xorg-video-nouveau`

Tercero. Si fuera necesario, desinstala el driver Nividia y elimina su configuración.
* `apt —purge remove nvidia-driver`
* `sudo apt remove nvidia*`

## Lectura adicional
* [Fuente AskUbuntu](https://askubuntu.com/questions/12937/remove-nvidia-driver-and-go-back-to-nouveau)
* [Freedesktop](https://nouveau.freedesktop.org/wiki/DebianInstall/)
* [Lista de controladores Nvidia](http://www.nvidia.com/object/unix.html)
* [Problemas al cargar Deepin]({{ site.url }}{{ site.baseurl }}/deepin/badload/).
* [Solución al controlador Wifi]({{ site.url }}{{ site.baseurl }}deepin/wififix/).
