---
date: 2017-11-17
type: Document
title:  "Revertir a controlador Nouveau (Nvidia)"
description: "Cómo arreglar la tarjeta Nvidia mal instalada"
tags:
    - deepin-tips y trucos
    - driver
    - nvidia
    - boot
---
Tenemos un vídeo explicando la instalación de Vulkan en un controlador de código abierto de Nvidia.

<div class="video_wrapper">
  <iframe src="https://www.youtube.com/embed/-pEgexpeNRs?rel=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
