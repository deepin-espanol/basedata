---
date: 2017-11-17
type: Document
title:  "Solución a los problema de DPI en controladores de Nvidia"
description: "Aplica a versiones propietarias"
tags:
    - deepin-tips y trucos
    - driver
    - nvidia
    - boot
---

La siguiente página proviene del portal [Lignux](https://lignux.com/solucionar-problemas-de-dpifuentes-pequenasal-instalar-los-drivers-privativos-de-nvidia/), parte del texto está licenciado bajo [Creative Commmons-Atribución-CompartirIgual versión 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

Si después de instalar los controladores propietarios el tamaño del texto en Deepin está reducido tenemos una solución para corregir la resolución de puntos por pulgada (o DPI, en inglés). Esto aplica a controladores de Nvidia, para más información visita [Dudas frecuentes sobre componentes]({{ site.url }}{{ site.baseurl }}/deepin/faq-componentes-gnulinux/).

1. Editamos el archivo `/etc/X11/xorg.conf.d/20-nvidia.conf`
  1. De forma rápida en la terminal `sudo nano /etc/X11/xorg.conf.d/20-nvidia.conf`
2. Cerca del apartado “Device” añadimos dos líneas
  - `Option "UseEdidDpi" "False"`
  - `Option "DPI" "96 x 96"
3. Guardamos el archivo y reiniciamos la PC

## Lectura adicional
* [Fuente](https://lignux.com/solucionar-problemas-de-dpifuentes-pequenasal-instalar-los-drivers-privativos-de-nvidia/)
* [Problemas al cargar Deepin]({{ site.url }}{{ site.baseurl }}/deepin/badload/).
* [Solución al controlador Wifi]({{ site.url }}{{ site.baseurl }}deepin/wififix/).
