---
date: 2017-11-17
type: Document
title: "Tips sobre GParted"
description: "Usar la terminal sin abrir el entorno gráfico"
tags:
  - deepin-tips y trucos
  - tty
  - terminal
  - destacado
---

<!--more-->

## Qué es GParter

GParter es una aplicación que administra las particiones del disco duro. encargada de limpiar la caché de las aplicaciones y el sistema.

## Consejos

Las mayoría particiones Linux llevan el sistema de archivos recomendado ext4. Sin embargo, las particiones de intercambio deben estar formateados a linux-swap. Cada partición lleva una denominación que comienza con "/dev/sd" como "/dev/sda1", "/dev/sda2" y así...

El punto de montaje tiene que ver la forma que Deepin use los recursos. "/" es la raíz del sistema operativo, "/home" la carpeta de usuario o "Mis documentos", "/opt" para las aplicaciones de terceros, etcétera.

Las particiones para Windows son formateadas bajo NTFS, mientras que el resto lleva como el universal FAT32. La partición de arranque, se encarga arrancar varios sistemas operativos; si se instala exclusivamente Deepin es obligatorio tener como punto de montaje "/boot".

<div class="video_wrapper">
        <iframe src="https://www.youtube.com/embed/H-9KUpDUM_8?rel=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>

Fuente: [Alex Tuts](https://www.youtube.com/channel/UC_WBYm61ZBy7jkw0u8_3-8g)
