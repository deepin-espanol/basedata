---
date: 2017-11-17
type: Document
title:  "Crear disco de arranque"
description: "Cómo instalar y usar esa característica"
comments: yes
tags:
    - deepin-tips y trucos
    - fix
    - dialogo
    - apps
---

Este apunte explica como crear una unidad de arranque desde la [terminal]({{ site.url }}/deepin/terminal/).

## Conocer particiones (USB)
Antes de crear la unidad de arranque, recomendamos revisar las particiones disponibles con el comando `pv`:
1. Si no tienes instala con `sudo apt install pv`
2. Ejecutamos `lsblk` antes de colocar la unidad USB
3. Colocamos el USB y volvemos a ejecutar `lsblk`
4. La unidad que se agregó a la lista (digamos `sdc`) viene a ser la partición que vas a crear la unidad de arranque

## Localizar la imagen ISO
1. Si lo descargaste en la sección de descarga tienes que buscar con `cd /home/[nombredeusuario]/Descargas`, si está en otra carpeta te aconsejamos a [conocer esta herramienta]({{ site.url }}{{ site.baseurl }}/deepin/archivos-terminal/).
2. Comprueba si está la imagen disco con `ls`
3.

## Pasos para crear unidad de arranque


## Fuentes
* [Lignux](https://lignux.com/usb-booteable-con-gnu-linux-desde-la-terminal/)
