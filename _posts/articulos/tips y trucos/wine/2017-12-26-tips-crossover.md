---
date: 2017-11-17
type: Document
title: "Tips sobre Crossover"
description: "Propuestas para instalar y ejecutar programas"
tags:
  - deepin-tips y trucos
  - wine
  - windows
  - conexión
---

Esta página es un complemento a [al tutorial para instalar aplicaciones terminadas en exe]({{ site.url }}{{ site.baseurl }}/deepin/exeapps). La idea es conocer con mejor detalle el mecanismo de instalación de aplicaciones para Microsoft Windows.

## Primeros pasos
### Sección principal
{% include imagen source="screenshots/crossover.png" alt="Crossover." %}

La selección de aplicaciones muestra si ellas están instaladas y las botellas que se aplicaron.

### Instalación
Para instalar una aplicación, haz clic en el ejecutable desde el gestor y selecciona "Instalar con Crossover" (o similar).

{% include imagen source="screenshots/crossover_preparacion.png" alt="Preparación en Crossover." %}
{% include imagen source="screenshots/crossover_instalacion.png" alt="Instalación en Crossover." %}

### Ejecutar un programa
Después de instalar, o si usas una versión portátil (portable), debes ejecutar desde la selección de aplicaciones (o el menú contextual). Tienes una ventana para que confirmes con la dirección equivalente a Windows.

{% include imagen source="screenshots/crossover_ejecutar.png" alt="Ejecutar un programa en Crossover." %}

## Información
### Carpeta Mis Documentos
Cada programa instalado en Crossover contiene una carpeta simiar a Mis Documentos. Debido a que las carpetas tienen una estructura especial, si quieres acceder a la carpeta de usuario de Deepin deberás hacer un acceso directo para manipular tus documentos.

### La unidad virtual
La carpeta de documento se encuentra en `~/.cxoffice/` (la carpeta `.cxoffice` de la carpeta de documentos).

Cada carpeta representa un programa y contiene lo siguientes:
* La carpeta `desktopdata` alberga los accesos directos
* La carpeta `dosdevices` contiene los accesos directos para las unidades montadas (evita redudancia).
* La cerpta `drive_c` contiene el directorio de la carpeta C. Solo está la información del programa. `C:/Program_Files/Nombre_de_aplicacion`.
* La cerpata `windata` contiene información e iconos del programa para acceder en Crossover o en el escritorio de Deepin.

### Botellas
* Crossover usa botellas como ayuda para ejecutar aplicaciones. Cada botella representa a uno de ellos, mediante el buscador puedes escoger cual botella puedes aplicar o crear uno de ellos.
* Para configurar una botella, selecciona una de ellas de la lista. Después accede a los Paneles de control.
* Puedes exportar o importar los cambios, la extensión es "cxarchive".

### Resolución de pantalla
* Algunos programas, sobretodo juegos, usa la pantalla completa. En varios monitores, puede generar malestar por la renderización.
  - Recomendamos cambiar el modo completo a ventana
  - Si ocurre algún malestar revisa la xorg.conf
* Más información: [Wiki](https://www.codeweavers.com/support/wiki/linux/faq/dualmonitorsexplained), [hilo de foro](https://www.codeweavers.com/support/forums/general/?t=25;forumcurPos=200;msg=97212)

### Instalación de paquetes
* Crossover usa otros paquetes para el renderizado de gráficos. Esa función es para convertir las interpretación de DirectX (nativas de Windows) a las de OpenGL (nativas de Linux).
* En ese caso, deberás revisar que paquetes serán instaladas.

{% include imagen source="screenshots/crossover_paquetes1.png" alt="Crossover." %}
{% include imagen source="screenshots/crossover_paquetes2.png" alt="Crossover." %}

### Otros detalles
* La carpeta donde se guardaba la información se ubicaba en `~/cxoffice/bin/cxsetup` (hasta la versión 16.).
* Para que tengas más control con la ejecución del programa, puedes revisar los procesos desde la selección de aplicaciones.

{% include imagen source="screenshots/crossover_procesos.png" alt="Procesos de Crossover." %}

## Instalar o ejecutar
* Si vas a instalar un programa, quedará almacenado en Crossover para que puedas acceder. Recomendamos esa opción.
* Si vas a ejecutar un programa, no creará un acceso directo a Crossover. Recomendamos usar para aplicaciones portables.
