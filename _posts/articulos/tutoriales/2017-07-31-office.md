---
date: 2017-11-17
type: Document
sidebar: right
title:  "Suite de ofimática en Deepin"
subheadline:  "Manual"
description: "LibreOffice o WPS."
tags:
    - deepin-tutoriales
    - wps
    - libreoffice
    - office
---
<!--more-->
En Deepin tienes varias aplicaciones para usar la ofimática. Dichas aplicaciones funcionan para procesamiento de texto, hojas de cálculo y diapositivas.

* ** [WPS]({{ site.url }}/deepin/usar-wps/) **. Incluida desde [Deepin Store]({{ site.url }}/deepin/wps/). Desarrollado por Kingsoft. Es gratuita.
* **LibreOffice**. Instalable también en [Deepin Store]({{ site.url }}/deepin/libreoffice/). Desarrollado por the Document Fundation. Es gratuita y de código abierto.
* **Microsoft Office**. Disponible para la versión de Windows, para eso tendrás que usar [un programa que interprete este sistema operativo]({{ site.url }}/deepin/exeapps/) (mejor dicho, usar Wine y derivados).

Puedes usar cualquiera de estas.

## Lectura adicional
* Post de [Alejandro en Google+](https://plus.google.com/+AlejandroCamarena/posts/LSBhef4DBxo) para instalar los paquetes más recientes de LO.
