---
type: Document
title:  "Problemas al arrancar Deepin"
description: "Intentando reparar Deepin"
tags:
    - deepin-tutoriales
    - terminal
    - recuperación
    - seguridad
    - datos
---

En esta página recopilamos los problemas ocurridos por un mal arranque en Deepin. Es una sección derivada de los [Problemas en Deepin]({{ site.url }}{{ site.baseurl }}/deepin/problemas-so/). La mayoría de veces ocurre al mostrar solo el texto segundos después de encender el equipo.

Necesitas conocer un [poco sobre la terminal]({{ site.url }}{{ site.baseurl }}/faq-que-es-terminal/) y tener los permisos de administrador para esos procedimientos.

## Cómo es el problema
En general, al iniciar Deepin te muestra este aviso:
~~~
You are in emergency mode...
Press Enter to continue
~~~

Si buscas el motivo del problema en una o más líneas, mejor. Para eso te recomendamos tener un registro de cambios ocurridos al sistema operativo y utilizarlo en casos de emergencia.

## Primeros pasos
Tenemos una formula para solucionar la mayoría de los problemas. Eso resumimos en tres palabras: **Recovery-Respaldo-Terminal**.

### Deepin Recovery
Deepin Recovery es un sistema operativo integrado en casos de emergencia. Incluye la mayoría de aplicaciones con opciones sencillas para reparar el equipo.

* Reparación de arranque
* [Deepin Clone]({{ site.url }}{{ site.baseurl }}/deepin/deepin-clone/)
* [GParted]({{ site.url }}{{ site.baseurl }}/deepin/gparted/)
* Terminal (funciones avanzadas)

### Copia de seguridad
Muchos problemas están relacionadas con una mala actualización o problema de rendimiento del equipo. Si has realizado una copia de seguirdad usando Deepin Clone (para Deepin 15.5 o superior) o similar, podrás revertir el problema.

Además puedes trasferir la información del respaldo a otro equipo. [Tenemos más información al respecto]({{ site.url }}{{ site.baseurl }}/deepin/faq-respaldo-particion/).

### Modo terminal
Una solución más avanzada es mediante la terminal. Si estás usando Deepin Recovery, puedes acceder en la terminal con la contraseña de administrador.

De lo contrario, te quedará usar la opción [a pantalla completa]({{ site.url }}{{ site.baseurl }}/deepin/tty/), sin gráficos y con mucha habilidad.

## Algunos puntos importantes
### Iniciar Recovery o modo terminal
* Desde el menú de arranque selecciona "Deepin Recovery"
* Para el modo YTT, al iniciar selecciona "recovery mode" en el modo avanzado escoge "root" y tendrás la terminal.

### Símbolos de numeral y dólar en la terminal
Representa si eres usuario [root]({{ site.url }}{{ site.baseurl }}/deepin/usuarios-root/) o no. El símbolo '$' es de un usuario normal y '#' cuando eres administrador o root. Puedes activar esos privilegios con el comando `su` y escribiendo la contraseña.

### Conectarse a Internets
No es obligatorio al usar la terminal. Sin embargo, lo necesitarás cuando descargues los paquetes de los repositorios. Recomendamos hacerlo vía cableado.

Si quieres configurar la IP o el [puente DNS]({{ site.url }}/deepin/lista-dns/) usa el comando:

~~~
sudo ifconfig
~~~

### Reparar paquetes dañados
Primero comprueba si hay paquetes dañados. Estos comandos sirven para instalar los paquetes incompletos en Deepin y elimina los inservibles (o huérfanos):
~~~
sudo apt install -f && sudo apt autoremove
~~~

Después para que puedas gestionar los paquetes recomendamos abrir la aplicación `aptitude`:

~~~
sudo aptitude
~~~

### Volver a actualizar Deepin
Si quieres hacer en este momento prueba estos comandos, eliminar la caché, actualizar los paquetes e instalar sin preguntar:
~~~
sudo apt-cache clean && sudo apt update && sudo apt full-upgrade -y
~~~

En cambio si tienes problemas con `dpkg` porque no responde:

~~~
sudo dpkg –configure -a
~~~

Mira [está página para actualizar la terminal]({{ site.url }}{{ site.baseurl }}/deepin/actualizar-terminal/).

### Recuperar el controlador de NVidia
Si tienes problemas con NVidia, por una mala instalación con el controlador propietario, visita [está página para instalar el controlador libre Nouveau]({{ site.url }}{{ site.baseurl }}/deepin/badload-nvidia/).

### Reparar partición dañada
Si Deepin tiene la partición dañada invitando a usar `fsck` [te recomendamos repsar en Dudas frecuentes]({{ site.url }}{{ site.baseurl }}/deepin/faq-reparar-particion/).

### Gestor de archivos
Estos pasos son para usar este gestor en lugar del tradicional [programa de Deepin]({{ site.url }}{{ site.baseurl }}/deepin/filemanager/).

Para ver y abrir los archivos puedes ver el comando `cd`:

~~~
cd [carpeta o nombre del archivo]
cd ..
~~~

Para ver los archivos usamos `ls`:

~~~
ls -l
~~~

Mira [está página para conocer otras formas de gestionar archivos desde la terminal]({{ site.url }}{{ site.baseurl }}deepin/archivos-terminal/).

## Lectura adicional
* [Pregunta en Stackexchange](https://unix.stackexchange.com/questions/46628/not-able-to-lock-var-lib-dpkg-lock-read-only)
* [Pasos para ser root en Wikihow](https://es.wikihow.com/ingresar-como-usuario-root-en-Linux)
