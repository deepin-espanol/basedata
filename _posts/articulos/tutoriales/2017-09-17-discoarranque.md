---
date: 2017-11-17
type: Document
title:  "Elaborar disco de arranque"
description: "Elabora el instalador en tu USB o DVD"
tags:
    - deepin-tutoriales
    - arranque
    - cd
    - dvd
    - usb
    - disco
---

Un disco de arranque contiene la información para que sea posible [instalar]({{ site.url }}/instalacion/). Aquí presentamos varias opciones, incluida la fácil, para DVD y USB. Esta opción es universal.

Después de  Deepin en una computadora de mesa o portátil requiere de una unidad de arraaque ([Boot disk](https://en.wikipedia.org/wiki/Boot_disk)).
En Deepin en Español te enseñamos a hacerlo.

## Requisitos

* Cualquier dispositivo de almacenamiento:
  - USB con 4 GB de almacenamiento mínimo.
  - Un DVD. No funciona con CD.
* Cualquiera de esos programas:
  - [Rufus](https://rufus.akeo.ie/)
  - [Unetbootin](https://unetbootin.github.io/)
  - [Deepin Boot Maker](https://www.youtube.com/watch?v=OzP_l9uCKbM) (disponible en Deepin Store)
  - También puedes usar con la terminal, los detalles más abajo

Nota: Si usas Rufus en Windows, recomendamos la versión portátil que pesa unos megabytes.

## Pasos

### Deepin Boot Maker (USB y disco)
1. Selecciona el archivo deepin-xx.iso (el nombre varía)
2. Escoge el dispositivo de almacenamiento
3. Confirma y espera unos minutos

{% include imagen source="screenshots/arranque.png" alt="Imagen de arranque." %}

### Rufus (solo USB)
1. Selecciona el dispositivo;
2. Haz clic en el icono al costado de "Imagen ISO" y selecciona el archivo deepin-xx.iso (el nombre varía)
3. Deja la configuración recomendada:
  - Partición GPT para computadora UEFI
  - Sistema de archivos FAT32
  - Las etiquetas "Crear disco de arranque" y "Formateo rápido" marcadas
4. Pulsa "Empezar"

### Unetbootin (USB y disco)
1. Selecciona "Imagen de disco" y selecciona el archivo
2. Selecciona el tipo "USB Drive" o "CD" y la unidad
3. Acepta

{% include imagen source="screenshots/unetboot.png" alt="Unetbootin." %}

### Terminal
Este método está pensado en los usuarios frecuentes con la terminal y equipos de limitados recursos: [Visita esta página]({{ site.url }}/deepin/darranque-terminal/)
