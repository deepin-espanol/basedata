---
date: 2017-11-17
type: Document
title:  "Arranque doble entre Deepin y Windows"
description: "Es posible convivir dos sistemas operativos, incluso con Secure Boot."
tags:
    - deepin-tutoriales
    - windows
    - dual-boot
    - particion
---

Cuando instalas Deepin, puedes conservar las particiones y el arranque junto a Windows. Esta opción te permitirá usar ambos software para diferentes necesidades.

## Recomendaciones después de instalar
### Instalar después de Windows
La forma de instalar Deepin sin borrar el disco duro es mediante [Deepin Installer]({{ site.url }}/deepin/deepininstaller/).

### Particiones
Para conocer mejor las particiones y su utilidad en los sistemas operativos, visita [la documentación para conocer y saber cómo formatear]({{ site.url }}/formatear/).

Haz una **copia de seguridad** de las particiones importantes y consérvalo en un disco duro aparte.

### Paquetes
Si usas un gestor de paquetes como [Synaptic]({{ site.url }}/deepin/synaptic/).
* Después de instalar Deepin tendrás preinstalado `shim`, encargado de ejecutar bajo Secure Boot. No lo borres.
* Revisa si el GRUB está correctamente configurado. En la terminal puedes actualizar con `sudo update grub`.

### Unidades de disco y Documentos
Dos puntos claves al usar archivos entre Deepin y Windows:
* Deepin usa el [gestor de archivos]({{ site.url }}/deepin/filemanger/) integrado (análogo al "Explorador de Windows").
* Si en Windows usa las letras de abecedario como "C" o "D", en Deepin usa "sda1", "sda2" como la ID de la unidad de disco...
* Para arrancar Deepin usa puntos de montaje `/` (principal) y `/home` (carpeta de usuario). Este último es similar a la Biblioteca en Windows 7 y contiene subcarpetas como Documentos, Imágenes, Descargas, entre otros.
* Puedes revisar con [GParter]({{ site.url }}/deepin/gparter/) para gestionar las particiones como harías en Microsoft Windows.

## Problemas frecuentes
* Si Deepin no consigue leer las particiones de Windows:
  - Deepin usa particiones EXT4 mientras que Windows usa NTFS. Por lo tanto, algunas operaciones avanzadas difieren en esos sistemas operativos.
  - Si has reiniciado incorrectamente Windows con el inicio rápido, las particiones serían bloqueadas. Una solución es desmarcar "Activar Inicio rápido (Recomendado)" en la Configuración de apagado del Panel de control.
  - Otra solución es crear una partición compartida FAT para almacenar archivos de hasta 4 GB.
* Si Deepin no arranca:
  - Una de sus particiones podría estar dañada. Al mostrar la pantalla con puro texto, usa la herramienta [fsck](https://es.wikipedia.org/wiki/Fsck) y ejecuta ´fsck /dev/sdXX -y´, por ahora comprobará algún daño y lo reparará.
  - Revisa si alguna aplicación de Windows está modificando las particiones de Deepin.
* Si Windows no arranca:
  - Investiga alguna irregularidad, existen ciertas posibilidades de ocurrir fallos en la unidad de arranque.
  - Revisa si el GRUB está dañado, más detalles en [GRUB]({{ site.url }}/deepin/grub/).

## Lectura adicional
*  [Publicación sobre Windows 10 Aniversary en MuyLinux](http://www.muylinux.com/2016/08/05/windows-10-anniversary-update/)
*  [Publicación sobre Windows 10 Aniversary en Fayerwayer](https://www.fayerwayer.com/2017/09/bug-de-windows-10-causa-perdida-de-archivos-en-dispositivos-android/).
*  [Publicación sobre el Inicio rápido en HowtoGeek](https://www.howtogeek.com/243901/the-pros-and-cons-of-windows-10s-fast-startup-mode/).
