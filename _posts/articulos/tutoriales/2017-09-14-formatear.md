---
date: 2017-11-17
type: Document
title:  "Formatear particiones"
description: "Organizando archivos en particiones"
tags:
    - deepin-tutoriales
    - tienda
    - aplicaciones
    - store
---

En Deepin en Español te enseñamos a [instalar]({{ site.url }}/instalacion/). Pero si tienes problemas al organizar los archivos, no necesitas volver a reinstalar.

Para tal caso necesitarás una aplicación encargada de particiones como [GParter]({{ site.url }}/deepin/gparted/) o "dd" en la terminal. Recuerda que necesitarás permisos de administrador y haber tenido una copia de seguridad para evitar problemas (como [Deepin Clone]({{ site.url }}/deepin/deepin-clone/)).

## Vistazo
### Qué es una partición
Una partición es un segmento de la unidad de disco donde almacena información. Una partición se caracteriza por:
* Tipo de partición (EXT, FAT, NTFS, etcétera)
* Nombre y etiqueta (la ID y el "nombre" que prefieres llamar)
* Montaje (si está activado podrás usarlo)

{% include imagen source="screenshots/menuc-particiones.png" alt="Siempre puedes ver la información desde el menú contextual (GParted)." %}

### Cómo es una partición
Dependiendo de la aplicaciones que uses, debes establece la particiones en cuestión. Para este caso debes conocer algunos puntos:
* La partición que creas estárá formateado bajo EXT4, una alternativa de gestión de datos a NTFS.
* Para que el sistema pueda localizar que carpeta usuará, asignamos a cada partición un "punto de montaje".
  - "/" para el sistema operativo
  - "/home" para la carpeta del usuario
  - "/boot/" para el arranque del sistema (lo explicaremos en el siguiente punto)
* La información de arranque (o boot) es necesaria para que el sistema operativo se ejecute.
  - Deepin usa su propio sistema de arranque.
  - Windows 10 también tiene su propio sistema de arranque pero funcionará en Deepin si instalas correctamente ([Deepin Instalelr]({{ site.url }}{{ site.baseurl }}/deepin/deepin-installer/)).
  - Si vas a instalar Deepin desde cero, recomendamos hacerlo en la unidad de disco donde estará el sistema operativo.
  - Más información en [este artículo]({{ site.url }}{{ site.baseurl }}/deepin/grub/)
* Opcionalmente, puedes uar [particiones de intercambio]({{ site.url }}/swap/) para extender la memoria virtual del equipo y evitar cuelges inesperado en equipos con pocos recursos.

{% include imagen source="screenshots/particion-swap.png" alt="Información de la partición Swap." %}

### Tabla de particiones
Cada disco duro tiene una tabla de particiones para que el disco "entienda" la ubicación de las particiones (además del arranque, los datos, entre otros). Si se daña la tabla, la informacion estaria en riesgo. Si quieres crear una tabla, recomendamos GPT en lugar de MBR.

Con esta presentación al formato, en la imagen ofrecemos un ejemplo para que estructures el disco duro el Deepin instalado.

{% include imagen source="screenshots/particiones-ejemplo.png" alt="Ejemplos de particiones." %}

## Formatear
Al instalar Deepin, podrás formatear las unidades de disco. Debes tener al menos una partición para los archivos del sistema.

### Casos especiales
* Las unidad externas, usan el formato FAT32 para que sean fáciles de trasladar información. Además sirven para la unidad de instalación del sistema operativo o repararlo en una emergencia.
* En GParted, si la partición está dañada usa la opcion de "Recuperar datos" con el botón secundario a una partición.

## Lectura adicional
* [Planeta Fedoraa](https://planetafedora.wordpress.com/acerca-de-particiones-y-puntos-de-montaje-tipos-de-formato-y-tamanos/)
* En Ayuda de Deepin
  - [Tips sobre GParted]({{ site.url }}{{ site.baseurl }}/deepin/tips-gparted/)
  - [Cifrar documentos]({{ site.url }}{{ site.baseurl }}/deepin/cifrar-docs/)
  - [Partición o archivo Swap]({{ site.url }}{{ site.baseurl }}/deepin/swap/)
  - [GRUB]({{ site.url }}{{ site.baseurl }}/deepin/grub/)
  - [Arranque doble]({{ site.url }}{{ site.baseurl }}/deepin/dual-boot/)
