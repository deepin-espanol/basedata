---
date: 2017-11-17
type: Document
sidebar: right
title:  "Usando WPS"
subheadline:  "Manual"
description: "Desde uso de documentos, seguido de hojas de cálculo y presentaciones."
tags:
    - deepin-tutoriales
    - wps
    - doc
    - Xls
    - ppt
    - word
    - excel
    - powerpoint
---
<!--more-->
En esta página te enseñamos a usar WPS para crear y editar documentos. WPS es una [suite ofimática]({{ site.url }}/deepin/office/) preinstalada en Deepin (para la versión 15.5).

## Programas
Estos son las aplicaciones correspondientes a la suite. Las siglas WPS tienen que ver con los nombres de:
* WPS Writer: Es un procesador de textos. Soporta exportación a PDF.
* WPS Spreadsheets: Es un programa para hojas de cálculo.
* WPS Presentation: Es un programa para realizar diapositivas.

Equivalencias:
* WPS Writer: Microsoft Word, LibreOffice Writer.
* WPS Spreadsheets: Microsoft Excel, LibreOffice Calc.
* WPS Presentation: Microsoft PowerPoint, LibreOffice Impress.

<div class="video_wrapper">
  <iframe src="https://www.youtube.com/embed/9kZ5HWRT0pI?rel=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>

## Pasos básicos
En general, las funciones básicas están en el menú de la aplicación:
* Crear nuevo documento: Seleccionar "Nuevo"
* Abrir documento: Seleccionar "Abrir"
* Guardar documento: Seleccionar "Guardar"
* Abrir configuración: Seleccionar "Opciones"

{% include imagen source="screenshots/menu-wps.png" alt="Captura de la aplicaciones WPS." %}

### Interfaz de usuario

La interfaz de usuario (para enero de 2018), consta de acciones y pestañas. De izquierda a derecha, Inicio, Insertar, Diseño de página, Referencias, Revisar, Vista y Sección.

{% include imagen source="screenshots/wps-writer.png" alt="Captura de la aplicaciones WPS." %}                                                                                    

## Notas adicionales
* Cómo cambiar el idioma en WPS: Tenemos [un tip explicando ese cambio]({{ site.url }}/deepin/cambiar-idioma-wps/)
* Añadir tipografía complementaria. Puedes instalar el paquete `ttf-wps-fonts` desde el gestor de paquetes. [un tip explicando ese cambio]({{ site.url }}/deepin/cambiar-idioma-wps/)
