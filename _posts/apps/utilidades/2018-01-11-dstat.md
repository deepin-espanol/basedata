---
date: 2017-11-17
type: Video
title: "dstat"
description: "Gestor de recursos"
tags:
    - apps-utilidades
    - memoria
    - recursos
    - terminal
video_id: ZZcZe9bUolc
---

dstat es una microprograma para terminal para gestionar los recursos.

## Sobre el servicio
Este servicio permite tener un control de los recursos que se gestionan. Ideal para desarrolladores

## Instalación

~~~
sudo apt-get install dstat
~~~

## Sinopsis

~~~
dstat [-afv] [options..] [delay [count]]
~~~

## Lectura adicional
* [linux.die.net](https://linux.die.net/man/1/dstat)
