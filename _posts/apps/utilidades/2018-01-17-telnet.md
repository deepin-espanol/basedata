---
date: 2017-11-17
type: Video
title: "Telnet"
description: "Accede a la terminal de forma remota"
tags:
    - apps-utilidades
    - terminal
    - remoto
    - telnet
video_id: z81lEJxPHiQ
resources:
  - name: Kenny Fabian
    link: https://www.youtube.com/channel/UC8QZG938WGQ14TavI19wUxw
---

Telnet es un servicio de emulación de terminal.

## Cómo ejecutar

~~~
telnet hostname
~~~

## Sinopsis

~~~
telnet [-468ELadr] [-S tos] [-b address] [-e escapechar] [-l user]
       [-n tracefile] [host [port]]
~~~

## Lectura adicional
* [Debian](https://packages.debian.org/es/sid/telnet)
* [Computerhope](https://www.computerhope.com/unix/utelnet.htm)
