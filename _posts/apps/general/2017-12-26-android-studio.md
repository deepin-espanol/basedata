---
type: Video
title: "Android Studio"
description: "Desarrollo de aplicaciones Android."
tags:
    - apps-general
    - android
    - google

video_id: f3-QM5--FNU
resources:
  - name: Andrew Ruffolo
    link: https://www.youtube.com/channel/UCTqkQCOfg2k27s4GJZWrkjg
---

## Qué es Android Studio
Android Studio es una aplicación para el desarrolllo de aplicaciones para el sistema operativo Android.

## Disponibilidad
Está disponible desde Deepin Store o su [sitio web](https://developer.android.com/studio/install.html). Para desarrollar en dispositivos móviles físicos, visita [ADB]({{ site.url }}{{ site.baseurl }}/deepin/adb/)
