---
type: Document
title: "Entangle"
description: "Gestiona la cámara desde la PC."
tags:
    - apps-general
    - película
    - multimedia
    - catálogo
---

## Qué es Alexandra
Alexandra Video Library es una videoteca ligera pensada en organizar contenido y reproducir desde la aplicación. Ideal para ver películas con la información de cada una de ellas.

La aplicación es portátil, ideal para ver contenido directamente desde el USB aunque no lleva un reproductor integrado. Los datos toman prestados del portal IMDB.

## Disponibilidad
Está disponible desde su [página web](http://melnik.solutions/project/alexandra).
