---
type: Video
title: "Darktable"
description: "Gestiona la cámara desde la PC."
tags:
    - apps-general
    - dslr
    - cámara
    - captura
    - remoto
    - videogaleria
video_id: sfBoekVDKkk
resources:
  - name: Juan Fernández
    link: https://www.youtube.com/channel/UC6l_8Mc9Vi_Mq74WSBTjZ3g
---

## Qué es Darktable
Darktable es una aplicación para la visualización y revelado de imágenes. Soporta RAW.

## Disponibilidad
Está disponible desde [Deepin Store](http://appstore.deepin.org/app/darktable). [Su enlace de descarga está en el sitio web)](https://www.darktable.org).
