---
type: Video
title: "JDownloader"
description: "Gestor de descargas."
tags:
    - apps-general
    - descargas
    - videoteca
    - web
video_id: _1EhsY4xmcw
resources:
  - name: Progressive ARG
    link: https://www.youtube.com/channel/UCsKOEexronOHvtmd3WtZhAg
---

## Qué es JDownloader
JDownloader es un gestor de descargas para varios servicios de la web. Soporta conectividad via proxy para evadir las restricciones por localización, resolvedor de CAPTCHA, programación de descarga, cuentas premium para varios servicios, etcétera.

Considera que **JDownloader no ofrece cuentas ni direcciones proxy**, tienes que conseguir por tu cuenta.

## Disponibilidad
Está disponible desde en Deepin Store o su [página web](http://jdownloader.org/download/index). Requiere Oracle Java.

El programa tiene un servicio para realizar actualizaciones, puedes hacerlo. Si tienes problemas, ingresa en la carpeta `/opt/jd2` con permisos de root y ejecuta el binario "Update" y después la aplicación.

El código fuente está [alojado en sus propios servidores](http://svn.jdownloader.org).
