---
type: Video
title: "Cerebro"
description: "Buscador integrado."
tags:
    - apps-general
    - android
    - google

video_id: AsSFcmoUGfA
resources:
  - name: Diolinux
    link: https://www.youtube.com/channel/UCEf5U1dB5a2e2S-XUlnhxSA
---

## Qué es Cerebro
Cerebro es una aplicación para realizar búsquedas rápidas como alternativa al [lanzador]({{ site.url }}{{ site.baseurl }}/deepin/panel/). Equivale al Spotlight de MacOS.

## Disponibilidad
Está disponible desde Deepin Store o su [sitio web](https://cerebroapp.com/).
