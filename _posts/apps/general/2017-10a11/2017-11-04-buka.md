---
type: Document
title: "Buka"
description: "Lector PDF y de historietas."
tags:
    - apps-general
    - libreria
    - pdf
    - comic
    - historieta
---

Buka es una aplicación para leer documentos e historietas.

## Disponibilidad

Está disponible en [Github](https://github.com/oguzhaninan/Buka/releases)

Fuente: Deepin en Español
