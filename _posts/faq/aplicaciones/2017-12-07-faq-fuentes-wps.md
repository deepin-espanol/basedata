---
title: Puedo cambiar el idioma de WPS
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - documentos
  - idioma
  - español
---

Es posible añadir más fuentes [WPS]({{ site.url }}{{ site.baseurl }}/deepin/wps). Prueba a instalar el paquete `ttf-wps-fonts` (`sudo apt install ttf-wps-fonts`).

Adicionalmente puedes instalar TrueType de Microsoft (conocido por ofrecer Calibri o Arial), cuyas fuentes son propietarias, con `sudo apt install ttf-mscorefonts-installer` y aceptar el contrato.

Fuente: [PCWorld](https://www.pcworld.com/article/2863497/how-to-install-microsoft-fonts-in-linux-office-suites.html)

{% include _faq_post.md %}
