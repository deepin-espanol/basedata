---
title: Me recomiendas distribución más ligera que Deepin
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - distro
  - ligera
---

Sí. Por alguna extraña razón, cuando tienes problemas en rendimiento recomendamos usar distribuciones que están optimizadas en la PC. **Recuerda, hazlo bajo tu responsabilidad.**

* Prueba un [sabor compatible]({{ site.url }}/deepin/sabores/). Existen sabores de Deepin como BigLinux que realiza modificaciones para hacer más ligera.
* O prueba Puppy Linux o PeeperMint. Son veteranas y de fácil uso.

{% include _faq_post.md %}
