---
title: Qué hacer si el controlador de Nvidia falla
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - controladores
  - nvidia
---

Es posible que se deba a una mala instalación con el programa de controladores de Deepin. Tenemos [unas propuestas para]({{ site.url }}/deepin/badload-nvidia/) instalar el controlador [Noveau]({{ site.url }}{{ site.baseurl }}/deepin/revertir-controlador-nvidia/), [Bumbleebee]({{ site.url }}/deepin/install-bumblebee-nvidia/). o el [ejecutable propietario]({{ site.url }}/deepin/install-ejecutable-nvidia/).

Además, si tienes problemas con el texto [presta atención a esta solución]({{ site.url }}/deepin/dpi-nvidia/).

Visita [está página para conocer los controladores exclusivos disponibles]({{ site.url }}/deepin/videocard/).

{% include _faq_post.md %}
