---
title: Qué hacer si no puedo iniciar Deepin después de ser instalado
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - instalación
  - arranque
---

Posiblemente sea una mala instalación, las particiones no fueron correctamente creados o hubo una interrupción inesperada. Si eso sucede, tenemos [una página de emergencia]({{ site.url }}{{ site.baseurl }}/deepin/badload).

Recuerda revisar si el disco duro tiene problemas de electricidad. Si sucede reemplázalo lo pronto posible e instálalo en esa unidad.

{% include _faq_post.md %}
