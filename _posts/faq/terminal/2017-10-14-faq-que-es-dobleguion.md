---
title: Qué es el doble guión
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - terminal
---

El doble guión es una solución al tradicional en algunos casos de la terminal:
* Eliminar un archivo que empieza con el guión medio: `rm -- -archivo.odt`
* Para la ayuda de una aplicación `--help`
* Cuando no funciona el guión largo
* Entre otros

Créditos: [Linuxito](https://www.linuxito.com/gnu-linux/nivel-basico/393-como-borrar-un-archivo-si-su-nombre-comienza-con-guion-del-medio), [Omar](https://314159bits.wordpress.com/2013/05/30/una-curiosidad-en-linux-con-los-archivos-cuyo-nombre-empieza-con-diagonal/)

{% include _faq_post.md %}
