---
title: Es necesario anteponer sudo
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - general
  - terminal
---

No estás obligado. Al ejecutar algunas shells para ver archivos, por ejemplo, basta escribir el "nombre del programa". Sin embargo, cuando algunos "programitas" necesitan editar partes críticas del sistema es obligatorio.

Si quieres realizar actividades críticas sin tener que escribir `sudo` cada cierto tiempo, escribe en esa línea `su` y confirma con la contraseña.

Fuente: [Element2048](https://element2048.wordpress.com/2007/02/12/el-comando-su-sudo-y-root/)

{% include _faq_post.md %}
