---
title: Qué es sudo
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - general
  - terminal
---

Es un juego de palabras relacionadas a "Superuser Do" para conceder como "root". Se debe anteponer `sudo` antes de la frase.

Secuencia: `sudo [nombre del shell] [nombre de la orden]`

Ejemplo: `sudo apt update`

Fuente: [Element2048](https://element2048.wordpress.com/2007/02/12/el-comando-su-sudo-y-root/)

Más información: [Anteceder sudo]({{ site.url }}/deepin/faq-anteceder-sudo/)


{% include _faq_post.md %}
