---
title: Qué es y para qué los shell
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - general
  - terminal
---

Dentro de la [terminal]({{ site.url }}/deepin/faq-que-es-terminal/), las órdenes son realizadas por el intérprete o shell. No es lo mismo con las aplicaciones gráficas que usan una librería gráfica.

Algunos shells vienen integrados en Deepin, otros requieren ser instalados desde los respositorios: `sudo apt install [nombre del shell]`.

{% include _faq_post.md %}
