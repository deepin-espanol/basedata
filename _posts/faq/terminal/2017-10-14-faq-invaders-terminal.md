---
title: Cómo conseguir el juego Invaders en la terminal
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - terminal
  - juego
---

Si tienes curiosidad, puedes jugar un juego en la terminal mientras realizas una actividad para ello deberás instalar Open Invaders (es el clon de código abierto de Space Inveders):

~~~
open-invaders
~~~

Si no has instalado, escribe lo siguiente:

~~~
sudo apt install open-invaders
~~~

{% include _faq_post.md %}
