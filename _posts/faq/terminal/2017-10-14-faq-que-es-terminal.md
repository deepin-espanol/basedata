---
title: Qué es y para qué sirve la terminal
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - general
  - terminal
---

El terminal es un componente básico que funciona al iniciar el sistema operativo. Desde los primeros años es parte importante para realizar tareas. Debido a las limitaciones de la época, este entorno tiene una intefaz basada en letras, símbolos y espacios.

Una de las computadoras que aprovechó este componente fue el [Commondare 64](https://es.wikipedia.org/wiki/Commodore_64). Sin embargo, con la ya avanzada interfaz gráfica (GUI) de los años 2000, la terminal quedó en segundo plano para el usuario común.

Nota curiosa: Si crees que usar la terminal te hará poderoso como en [Hackers](https://en.wikipedia.org/wiki/Hackers_(film)), en donde las personas pueden romper el sistema del Estado con escribir códigos, te equivocas. Entiende los riesgos que conlleva y los correctos usos con los comandos.

Más información: [Qué es shell]({{ site.url }}/deepin/faq-que-es-shell/)

{% include _faq_post.md %}
