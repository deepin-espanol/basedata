---
type: Document
title:  "Qué hacer si me avisa que no puedo leer particiones de Windows"
description: "Parte de la FAQ de Ayuda de Deepin"
tags:
    - deepin-faq
    - windows
    - avisos
---

Eso se debe a la activación de la característica Inicio rápido de Microsoft Windows (específicamente la versión 10 y superiores). Eso ocasionaría una restricción con la partición en Deepin.

{% include imagen source="screenshot/arranquerapido-deepin.jpg" alt="Aviso de Arranque rápido en Deepin." %}

Intenta iniciar Windows, haz un reinicio y vuelve a abrir Deepin. Si el problema persiste, desactiva el "Inicio rápido". Revisa [la página sobre arranque doble]({{ site.url }}{{ site.baseurl }}/deepin/dual-boot) para más información.

{% include _faq_post.md %}
