---
type: Document
title:  "Me dice que no tengo Java instalado, cómo lo hago"
description: "Parte de la FAQ de Ayuda de Deepin"
tags:
    - deepin-faq
    - java
    - instalación
---

Uno de los inconvenientes al ejecutar una aplicación es la necesidad de ejecutar en una máquina virtual de Java. Mejor dicho, si la aplicación está escrita en Java debes instalar el componente Java.

{% include imagen source="screenshots/instalarjava.jpg" alt="Instalando Java en Deepin." %}

Prueba a instalar `apt install oracle-javaX`, X es el número de versión mayor que vas a instalar. Para la versión 1.6, sería `sudop apt install oracle-java6`.

{% include _faq_post.md %}
