---
title: Cómo cambio el fondo de pantalla
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - wallpaper
  - terminal
---

Puedes cambiar el fondo de pantalla desde el centro de control (más detalles en [Personalización]({{ site.url }}/deepin/personalizacion/)). Si quieres añadir más fondos selecciona el icono "+"

Para los más experimentados, puedes conseguir más fondos descargando desde la terminal a la carpeta de `Wallpaper`.

~~~
mkdir wallpaper
wget -nd -r -P wallpaper -A jpeg,jpg,bmp,gif,png [sitio web]
~~~

Prueba con páginas web como `http://www.kinyu-z.net/`. O también ejecutando scripts como [National Geographic](https://gist.github.com/joshschreuder/882668) o [Bing](https://loganmarchione.com/2014/12/bash-script-use-bings-background-wallpaper/).

{% include _faq_post.md %}
