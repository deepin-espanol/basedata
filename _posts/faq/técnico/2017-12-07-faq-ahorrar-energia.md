---
title: Cómo ahorrar y diagnosticar el consumo de energía
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - software
---

Puedes revisar el consumo de energía con el monitor de Deepin y puedes ahorrar energía desde el centro de control o con herramientas extras.

Echa un vistazo [al tip]({{ site.url }}{{ site.baseurl }}deepin/ahorro-energia/) para conocer las herramientas [`tlp`]({{ site.url }}{{ site.baseurl }}/deepin/tlp/) y similares que se encargan de optimizar el consumo.

{% include _faq_post.md %}
