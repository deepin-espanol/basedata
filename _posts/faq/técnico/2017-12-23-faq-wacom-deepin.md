---
title: Cómo instalo el controlador Wacom
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - tableta
  - wacom
---

Wacom tiene un controlador para ejecutar dispositivos de la marca de lapiceros y paneles digitales. Visita el [sitio web para conseguir el instalador](http://linuxwacom.sourceforge.net/wiki/index.php/Downloads).

Para instalar el controlador prueba ejecutando ese comando:

~~~
sudo apt install input-wacom
~~~

{% include _faq_post.md %}
