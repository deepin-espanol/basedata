---
title: Puedo usar particiones de Windows con Deepin
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - particiones
---

Sí, tal como menciona [el manual]({{ site.url }}/deepin/dual-boot/).

Si quieres conocer más detalles sobre la partición visita [Formatear particiones]({{ site.url }}{{ site.baseurl }}/deepin/formatear/)

{% include _faq_post.md %}
