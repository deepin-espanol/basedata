---
title: Qué es una partición
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - particiones
---

Una partición es un contenedor del disco duro que almacena datos. Las particiones pueden tener archivos del sistema o se apoyan de la caché para arrancar el sistema operativo.

Fuente: [Planeta Fedoraa](https://planetafedora.wordpress.com/acerca-de-particiones-y-puntos-de-montaje-tipos-de-formato-y-tamanos/)

Más información: [Formatear particiones]({{ site.url }}{{ site.baseurl }}/deepin/formatear/)

{% include _faq_post.md %}
