---
title: Cuántas particiones puedo tener en mi disco duro?
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - particiones
---

Eso dependerá de la estructura de disco duro. Para eso necesitarás de un sistema para gestionar particiones llamada "tabla". Por defecto se usa MBR, ***Registro de arranque principal*** en inglés, ya que ofrece más funciona que su antecesora GPT.

En esa tabla MBR tienes un máximo de 4 contenedores llamadas "particiones primarias". De las cuales cada una puede dividirse en particiones lógicas para el sistema, el arranque, los documentos, entre otros.

Fuente: [Planeta Fedoraa](https://planetafedora.wordpress.com/acerca-de-particiones-y-puntos-de-montaje-tipos-de-formato-y-tamanos/)

Más información: [Formatear particiones]({{ site.url }}{{ site.baseurl }}/deepin/formatear/)

{% include _faq_post.md %}
