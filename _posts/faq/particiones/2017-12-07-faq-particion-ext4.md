---
title: Porqué Deepin necesita particiones EXT4
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - particiones
---

En Gnu/Linux usamos el sistema de archivos extendido o EXT, de código abierto y libre de patentes. Fue presentado en 1992 con la primera versión de Linux. Un año después se implementaría su segunda versión para usar en particiones con varios giga-octetos (GB) de almacenamiento.

En 2008 se lanzaría la [versión 4](https://en.wikipedia.org/wiki/Ext4), abreviada EXT 4 con muchas mejoras. Theodore Ts'o es el encargado de desarrollar esa versión. Es estable, tiene menos posibilidad de fragmentarse, para particiones de hasta 16 terabytes y más de 4 mil millones de archivos cada uno.

Lejos de los detalles técnicos, el EXT4 es recomendable para particiones del sistema y los documentos ya que son fáciles de reparar, son rápidos de leer y reciben mejoras de rendimiento junto al núcleo Linux.

Nota: Este formato no aplica a Swap ni a dispositivos externos.

Más información: [Formatear]({{ site.url }}{{ site.baseurl }}/deepin/formatear/)

{% include _faq_post.md %}
