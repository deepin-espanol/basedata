---
title: Qué significa los puntos de montaje
description: "Parte de la FAQ de Ayuda de Deepin"
type: Document
tags:
  - deepin-faq
  - particiones
---

Para que el sistema pueda localizar que carpeta usuará, asignamos a cada partición un "punto de montaje".

Curiosamente el punto de montaje "/" es el equivalente a la unidad C al sistema Microsoft Windows. El punto "/home" es a la unidad D.

Más información: [Formatear particiones]({{ site.url }}{{ site.baseurl }}/deepin/formatear/)

{% include _faq_post.md %}
