---
name: Deepin Clone
repository: https://github.com/linuxdeepin/deepin-clone
demo_url: https://www.youtube.com/watch?v=Uq3HFUvrWBw
description: Copias de seguridad.
images:
  - image: /images/dsoftwares/deepin-clone/1.png
  - image: /images/dsoftwares/deepin-clone/2.png
  - image: /images/dsoftwares/deepin-clone/3.png
features:
  - Soporte para Deepin Recovery.
  - Aplica a discos enteros y particiones.
  - Clonación entre particiones.
order: 6
---
Deepin Clone es una herramienta para copias de seguridad. Además, de crear y respaldar en unidades ISO, soporta la clonación entre particiones. Las particiones del sistemas son realizadas en Deepin Recovery para reparar el sistema operativo.
