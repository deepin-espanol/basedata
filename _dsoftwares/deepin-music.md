---
name: Deepin Music
repository: https://github.com/linuxdeepin/deepin-music
demo_url: https://www.youtube.com/watch?v=lUC1ruOmCJE
description: Reproductor de música para Deepin.
images:
  - image: /images/dsoftwares/deepin-music/1.png
  - image: /images/dsoftwares/deepin-music/2.png
  - image: /images/dsoftwares/deepin-music/3.png
features:
  - Listas de reproducción.
  - Letras o cancioneros.
  - Vista de carátulas.
order: 8
---

Deepin Music es una aplicación enfocada en lo musical. Permite centralizar las canciones, separar en lista de reproducción y vista de letras (via Netease).
