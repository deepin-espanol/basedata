---
name: Deepin Movie
repository: https://github.com/linuxdeepin/deepin-movie
demo_url: https://www.youtube.com/watch?v=lUC1ruOmCJE
description: Reproductor de vídeo para Deepin.
images:
  - image: /images/dsoftwares/deepin-video/1.png
  - image: /images/dsoftwares/deepin-video/2.png
  - image: /images/dsoftwares/deepin-video/3.png
features:
  - Soporte multiformato, varios idiomas y subtítulos.
  - Sintonización en vivo y resolución 4K.
  - Lista de reproducción.
order: 7
---

Deepin Movie ofrece una interfaz de uso intuitiva con atajos de teclados. Soporta varios formatos (códecs) y sintonización de vídeo en directo.
