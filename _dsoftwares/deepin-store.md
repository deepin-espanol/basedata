---
name: Deepin Store
repository: https://github.com/linuxdeepin/deepin-appstore
demo_url: https://www.youtube.com/watch?v=ZnqEotDpNns
description: Ayuda para instalar Deepin.
images:
  - image: /images/dsoftwares/deepin-appstore/1.png
  - image: /images/dsoftwares/deepin-appstore/2.png
  - image: /images/dsoftwares/deepin-appstore/3.png
features:
  - Instalación en un clic.
  - Gestor de descargas.
  - Capturas de pantalla.
order: 5
---

Deepin Store es una tienda integrada para descargar e instalar aplicaciones. Por defecto, las aplicaciones son revisadas por el equipo. Para que las aplicaciones sean instaladas es necesario que el repositorio esté actualizado.
