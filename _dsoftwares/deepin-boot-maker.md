---
name: Deepin Boot Maker
repository: https://github.com/linuxdeepin/deepin-boot-maker
demo_url: https://www.youtube.com/watch?v=YzZEKpBQDIw
description: Creador de unidad de arranque.
images:
  - image: /images/dsoftwares/deepin-boot-maker/1.png
  - image: /images/dsoftwares/deepin-boot-maker/2.png
  - image: /images/dsoftwares/deepin-boot-maker/3.png
features:
  - Fácil de crear.
  - Multiplataforma.
order: 3
---
Deepin Boot Maker es un programa para la creación de arranque. Primero añades la unidad ISO y colocas la unidad DVD o USB con 4 GB de espacio disponible.
