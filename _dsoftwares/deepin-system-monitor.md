---
name: Monitor del sistema
repository: https://github.com/linuxdeepin/deepin-system-monitor
demo_url: https://www.youtube.com/watch?v=ZkWl64LBmo4
description: Controla las tareas de la PC.
images:
  - image: /images/dsoftwares/deepin-system-monitor/1.png
  - image: /images/dsoftwares/deepin-system-monitor/2.png
  - image: /images/dsoftwares/deepin-system-monitor/3.png
features:
  - Opción de suspender y finalizar aplicaciones
  - Identificador de procesos.
order: 4
---
Deepin Boot Maker es un programa para la creación de arranque. Primero añades la unidad ISO y colocas la unidad DVD o USB con 4 GB de espacio disponible.
